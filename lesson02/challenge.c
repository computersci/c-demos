#include<stdio.h>

int main() {
    float price = 0;
    int quantity = 0;

    printf("Please enter the price: ");
    scanf("%f", &price);
    printf("Please enter the quantity: ");
    scanf("%d", &quantity);

    float subtotal = price * quantity;
    printf("Subtotal: %f\n", subtotal);

    return 0;
}
