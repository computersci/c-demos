#include <stdio.h>

int main(int argc, char *argv[]) {
    // At this stage, argc will contain the number of arguments and
    // argv will contain the arguments themselves, as a list.

    // To access the arguments, you can use argv[0] for the first argument
    // argv[1] for the second and so on.

    // Note that the first argument, argv[0], is actually always filled with
    // the name of the program.

    // To print out all the arguments, you can use:
    for (int i = 0; i < argc; i++) {
        printf("[%2d] %s\n", i, argv[i]);
    }

    // Don't worry about what the 'for' bit does yet, we will cover that in
    // a future lesson

    return 0;
}

