#include <stdio.h>

int main() {
    // Create a buffer able to contain 64 characters of text
    char buf[64];

    // Read the input from the user into the buffer
    printf("Please enter some text: ");
    scanf("%s", &buf);

    // at this point, buf will contain the text the user typed in
    printf("You typed in: %s\n", buf);

    return 0;
}

