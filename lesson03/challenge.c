#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    float price = 0;
    int quantity = 0;

    if (argc == 3) {
        price = atof(argv[1]);
        quantity = atoi(argv[2]);
    } else {
        printf("Please enter the price: ");
        scanf("%f", &price);
        printf("Please enter the quantity: ");
        scanf("%d", &quantity);
    }

    float subtotal = price * quantity;
    printf("Subtotal: %f\n", subtotal);

    return 0;
}
