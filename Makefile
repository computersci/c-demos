all: clean build

build: lesson_one lesson_two lesson_three

lesson_one:
	mkdir -p out/lesson01
	gcc -Wall -o out/lesson01/challenge lesson01/challenge.c

lesson_two:
	mkdir -p out/lesson02
	gcc -Wall -o out/lesson02/challenge lesson02/challenge.c
	gcc -Wall -o out/lesson02/example01 lesson02/example01.c
	gcc -Wall -o out/lesson02/example02 lesson02/example02.c

lesson_three:
	mkdir -p out/lesson03
	gcc -Wall -o out/lesson03/challenge lesson03/challenge.c

clean:
	rm -rf out/

