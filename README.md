# C Demos

A set of C demonstrations for use with teaching. See the wiki for more details.

## Compiling

All demos can be built (on Linux) with `make`:

```sh
$ make
```

The build can be cleaned with:

```sh
$ make clean
```

To build a specific lesson, you can ask make to do so (convert the lesson number
to English as shown):

```sh
$ make lesson_two
```
